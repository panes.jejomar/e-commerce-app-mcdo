//dependencies and packages
const express = require('express');
const mongoose = require('mongoose');
const dotenv = require("dotenv");
const cors = require('cors');

//server setup
const app = express();
dotenv.config();
let port = process.env.PORT;
let secret = process.env.CONNECTION_STRING;
app.use(express.json());
app.use(cors());

//database connect
mongoose.connect(secret);
let dBStatus = mongoose.connection;
dBStatus.on('open', () => {
    console.log('Connected to database')
});

//gateway response
app.get('/', (req, res) => {
    res.send(`Welcome to Mcdo App`)
});

app.listen(port, () => {
    console.log(`Server is running  on port ${port}`)
})