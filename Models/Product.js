//dependencies and modules
let mongoose = require('mongoose');

//blueprint schema
let productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: Date()
    }
})

//exports
let Product = mongoose.model('Product', productSchema);
module.exports = Product;
