//dependencies and modules
let mongoose = require('mongoose');

//blueprint schema
let orderScema = new mongoose.Schema({
    totalAmount: {
        type: Number
    },
    purchasedOn: {
        type: Date,
        default: Date()
    },
    products: [{
        productId: {
            type: String,
            required: true
        },
        productsQuantity: {
            type: Number,
            required: true
        }
    }]
})

//module exports
let Order = mongoose.model('Order', orderScema);
module.exports = Order;
