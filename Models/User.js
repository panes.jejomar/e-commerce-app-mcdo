//dependencies and modules
let mongoose = require('mongoose');

//blueprint schema
let userSchema = new mongoose.Schema ({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    cart: [{
        productId: {
            type: String
        }
    }],
    order: [{
        orderId: String
    }]
})

//module exports
let User = mongoose.model('User', userSchema);
module.exports = User;